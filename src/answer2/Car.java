package answer2;

public class Car extends Vehicle{
	public Car(){
		super();
	}
	
	public Car(int petrol){
		super(petrol);
	}
	@Override
	public void getLimiteFill() {
		System.out.println("Limit petrol of car is 20 liters.");
		
	}
	
	public void getLimiteFill(int petrol) {
		System.out.println("Limit petrol of car is "+petrol+" liters.");	
	}

}
