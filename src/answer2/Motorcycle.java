package answer2;

public class Motorcycle extends Vehicle{
	
	public Motorcycle(){
		super();
	}
	
	public Motorcycle(int petrol){
		super(petrol);
	}

	@Override
	public void getLimiteFill() {
		System.out.println("Limit petrol of motorcycle is 5 liters.");
		
	}
	
	public void getLimiteFill(int petrol) {
		System.out.println("Limit petrol of motorcycle is "+petrol+" liters.");	
	}
}
