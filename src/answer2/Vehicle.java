package answer2;

public abstract class Vehicle {
	private int petrol;
	
	public Vehicle(){
		petrol = 0;
	}
	
	public Vehicle(int petrol){
		this.petrol = petrol;
	}
	
	public void move(){
		
	}

	public abstract void getLimiteFill();
}

