package answer2;

public class Truck extends Vehicle{
	public Truck(){
		super();
	}
	
	public Truck(int petrol){
		super(petrol);
	}
	@Override
	public void getLimiteFill() {
		System.out.println("Limit petrol of truck is 35 liters.");
		
	}

	public void getLimiteFill(int petrol) {
		System.out.println("Limit petrol of truck is "+petrol+" liters.");	
	}
}
