package answer2;

public class Main {

	public static void main(String[] args) {
		
		Motorcycle fino = new Motorcycle();
		fino.getLimiteFill();
		fino.getLimiteFill(10);
		
		Car fiesta = new Car();
		fiesta.getLimiteFill();
		fiesta.getLimiteFill(25);
		
		Truck benz = new Truck();
		benz.getLimiteFill();
		benz.getLimiteFill(40);
	}

}
