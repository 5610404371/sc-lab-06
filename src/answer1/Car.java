package answer1;

public class Car {
	private int petrol;
	
	//default constructor
	public Car() {
		petrol = 0;
	}
	
	//overload constructor
	public Car(int petrol) {
		this.petrol = petrol;
	}
	
}
