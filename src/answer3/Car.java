package answer3;

public class Car {
	private int petrol = 100;
	
	public Car(int petrol) {
		petrol = petrol; //here, the local variable has closer scope than the instance 
        // variable, so the expression set parameter equal to itself
		System.out.println("local variable is " + petrol);
		
		this.petrol += petrol;// this is the correct way to set the parameter to the 
        //instance variable.
		System.out.println("instance variable is " + this.petrol);
	}
	
}